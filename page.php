<?php
    abstract class BasePage{
        abstract public function render();
    }


    class Page extends BasePage{
        private $title;
        private $text;

        function setTitle($title){
            $this->title = $title;
        }

        function setText($text){
            $this->text = $text;
        }
        function render(){
            echo "<h1>$this->title</h1><p>$this->text</p>";

        }


    }
?>